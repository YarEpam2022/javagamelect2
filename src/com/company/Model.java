package com.company;

import java.util.Random;

public class Model {
    private int currentMax = 100;
    private int currentMin = 0;
    final private Random random = new Random();
    private int RandNumb = random.nextInt(100);
    private StringBuilder GameLog = new StringBuilder();
    private int MoveCount = 0;

    public boolean PlayerMove(int currentNumber){
        if(currentNumber == RandNumb){
            System.out.println("Поздравляю, вы выиграли!");
            GameLog.append(String.format("Игрок угадал число за %d ходов",MoveCount));
            return true;
        }
        else if(currentNumber < RandNumb && (currentNumber >= currentMin && currentNumber<= currentMax)){
            System.out.println("Введенное вами число меньше, чем загаданное");
            GameLog.append(String.format("Ход %d: Игрок ввел число %d, оно меньше загаданного - диапазон смещается до [%d,%d]\n",MoveCount,currentNumber,currentNumber,currentMax));
            currentMin = currentNumber;
            MoveCount++;
            return false;
        }
        else if(currentNumber>RandNumb && (currentNumber >= currentMin && currentNumber<= currentMax)){
            System.out.println("Введенное вами число больше, чем загаданное");
            GameLog.append(String.format("Ход %d: Игрок ввел число %d, оно больше загаданного - диапазон смещается до [%d,%d]\n",MoveCount+1,currentNumber,currentMax,currentNumber));
            currentMax = currentNumber;
            MoveCount++;
            return false;
        }
        else {
            System.out.println("Введено неправильное число, пожалуйста повторите попытку.");
            MoveCount++;
            return false;
        }
    }

    public int getCurrentMax() {
        return currentMax;
    }

    public int getCurrentMin() {
        return currentMin;
    }

    public StringBuilder getGameLog() {
        return GameLog;
    }

    public void setCurrentMax(int currentMax) {
        this.currentMax = currentMax;
    }

    public void setCurrentMin(int currentMin) {
        this.currentMin = currentMin;
    }
}
