package com.company;

import java.util.Scanner;

public class View {

    public int PlayerMove(int currentMax, int currentMin){
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите число в диапазоне от %d до %d\n",currentMin,currentMax);
        return scanner.nextInt();
    }

    public void ShowGameLog(StringBuilder GameLog){
        System.out.println(GameLog);
    }
}
