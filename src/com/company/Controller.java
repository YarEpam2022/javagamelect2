package com.company;

import java.util.Random;

public class Controller {
    private Model model;
    private View view;


    public Controller(Model model, View view){
        this.model = model;
        this.view = view;
    }

    public void Run(){
        while(!model.PlayerMove(view.PlayerMove(model.getCurrentMax(),model.getCurrentMin())));
    }
}
